console.log("Selamat Anda berhasil menggunakan JavaScript pada Website");
let objek = {key1: "value1", key2: "value2", key3: "value3"}
console.log(objek)
// let user = {firstName: "Harry", lastName: "Potter",  age: 20, isMuggle: false, stuff: ["Wand", "Flying Car", "Owl"]}; 
// console.log(user);
// console.log("Hallo, nama saya " + user.firstName + " " + user.lastName);
// console.log("Umur saya " + user.age + " tahun");
let user2 = {
    name: {
        first: "Harry",
        last: "Potter"
    },
    age: 20, 
    isMuggle: false,
    stuff: ["Magic Wind", "Flying Car", "Owl"]
}
console.log("Hallo, nama saya " + user2.name.first + " " + user2.name.last);
// end of object js
//array js
let myArray = ["Coklat", 42.5, 22, true, "Programming"];
console.log(myArray);
console.log(myArray[1]);
//end of array
//arimatika
let x = 10;
let y = 5
 
x += y; // artinya -> x = x + y;
x -= y; // artinya -> x = x - y;
x *= y; // artinya -> x = x * y;
x /= y; // artinya -> x = x / y;
x %= y; // artinya -> x = x % y;
// end og aritmatika
//looping
console.log(x);
for(let i = 1; i < 6; i++) {
    console.log(i);
}
const nama = ["Harry", "Ron", "Hermione", "Tom"];
 
for(let a = 0; a < nama.length; a++) {
    console.log(nama[a]);
}
for(const arrayItem of nama) {
    console.log('nama '+arrayItem)
}
//end of loop
// good alert
// const firstName = prompt("Siapa nama depanmu?");
// const lastName = prompt("Siapa nama belakangmu?");
// const language = prompt("Bisa berbahasa apa?");
 
// const user = {
//    name: {
//        first: firstName,
//        last: lastName,
//    },
//    language: language
// };
 
// if (user.language === "English") {
//    alert("Nice to meet you " + user.name.first + " " + user.name.last + "!");
// } else if (user.language === "French") {
//    alert("Ravi de vous rencontrer " + user.name.first + " " + user.name.last + "!");
// } else if (user.language === "Japanese") {
//    alert("Hajimemashite, " + user.name.first + " " + user.name.last + "!");
// } else {
//    alert("Senang bertemu dengan Anda " + user.name.first + " " + user.name.last + "!");
// }
// end of alert
const calculator = {
    displayNumber : '0',
    operator : null,
    firstNumber : null,
    waitingForSecondNumber : false
};

function updateDisplay() {
    document.querySelector('#displayNumber').innerText = calculator.displayNumber;
}

function clearCalculator() {
    calculator.displayNumber = '0';
    calculator.operator = null;
    calculator.firstNumber = null;
    calculator.waitingForSecondNumber = false;
}

function inputDigit(digit) {
    if(calculator.waitingForSecondNumber && calculator.firstNumber === calculator.displayNumber){
        calculator.displayNumber = digit;
    } else {
        calculator.displayNumber === '0' ? calculator.displayNumber = digit : 
        calculator.displayNumber += digit; 
    } 
}

function inverseNumber() {
    if(calculator.displayNumber === '0'){
        return ;
    }
    calculator.displayNumber = calculator.displayNumber * -1;
}

function handleOperator(operator) {
    if(!calculator.waitingForSecondNumber){
        calculator.operator = operator;
        calculator.waitingForSecondNumber = true;
        calculator.firstNumber = calculator.displayNumber
    } else {
        alert('operator sudah ditetapkan')
    }
}

function performCalculation() {
    if(calculator.firstNumber == null || calculator.operator == null){
        alert("Anda belum menetapkan operator");
    
        return;
    }

    let result = 0;

    if(calculator.operator === "+"){
        result = parseInt(calculator.firstNumber) + parseInt(calculator.displayNumber);
    } else {
        result = parseInt(calculator.firstNumber) + parseInt(calculator.displayNumber)
    }

    const history = {
        firstNumber : calculator.firstNumber,
        secondNumber : calculator.displayNumber,
        operator : calculator.operator,
        result : result
    }

    putHistory(history);
    calculator.displayNumber = result;
    renderHistory();
}

const buttons = document.querySelectorAll(".button");
for(let button of buttons){
    button.addEventListener('click', function(event){
        //mendapatkan objek elemen yang di klik
        const target = event.target;
        //clear
        if(target.classList.contains('clear')){
            clearCalculator();
            updateDisplay();
            return;
        }
        // end of clear

        //inverse
        if(target.classList.contains('negative')){
            inverseNumber();
            updateDisplay();
            return;
        }
        //end
        //equals
        if(target.classList.contains('equals')){
            performCalculation();
            updateDisplay();
            return;
        }
        //end
        //operator
        if(target.classList.contains('operator')){
            handleOperator(target.innerText);
            updateDisplay();
            return;
        }
        //end
        inputDigit(target.innerText);
        updateDisplay()
    })
}

if (typeof(Storage) !== "undefined") {
    // Browser mendukung sessionStorage/localStorage.
    console.log('support')
} else {
// Browser tidak mendukung sessionStorage/LocalStorage
    console.log('not support')
}